# UVS: Unnamed Vido Synth

This is a very silly name because this project doesn't really *synthesize* videos at all. Instead, it is a rough tool for translating and warping videos with p5.js. I usually use it as a part of my process between generated videos from [La Habra](https://gitlab.com/sarahghp/la-habra) and passes with a framebuffer emulator.

⚠️ Warning: this code exists but may not be maintained. It may have unexpected breaking changes. It makes no promises.⚠️

## Examples 

#### 1: A video
![plain video](img/01.gif)

#### 1: Wrapped 
![video with wrap applied](img/02.gif)

#### 1: Chunked
![video with chunk applied](img/03a.gif)
![video with chunk applied](img/03b.gif)

#### 1: Smeared
![video with smear applied](img/04.gif)

## How to Use 

### Setup

1. Run a [local server](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server#running_a_simple_local_http_server) on your machine.

2. Add the file path to `main.js` in the `vidTitle` declaration. 

### Keys 

Once the server is running, you can use the following keys to interact.

#### Mouse click 
Starts the video.

#### Space 
Toggles between transform mode and plain video play.

#### Arrow keys 
Change horizontal and vertical velocities. 

#### c, s, w
Mode choice: chunk, smear, wrap.

### Recording 
The canvas can be "recorded" by pressing the ⭕️ button. When you're done a link appears to download the file — it will look like a `.txt` but you can change the extension on your computer to `.mp4`. I recommend processing it with the `format` function in [ffmfriend](https://gitlab.com/sarahghp/ffmfriend) to compress the file.

This is a pain, though, so using Quicktime to do a screen recording is also a perfectly good option.