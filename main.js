import { recordAndAppend } from './modules/index.js'

// Global canvas -------
let c;

// Video input variables -------
let vid;
let vWidth = Math.floor(1920 * .6);
let vHeight = Math.floor(1080 * .6);
const vidTitle = 'movies/Womenswimming_silent.mp4';

const logVel = (text) => {
  document.getElementById('vel-text').innerText = text;
}

// Movement variables -------
let getVelocityUpDown = getFibonacciVelocity();
let getVelocityLeftRight = getFibonacciVelocity();
let globalVelocityScaler = 0.5;

let currentVelocity = {
  x: getVelocityLeftRight.next().value,
  y: getVelocityLeftRight.next().value,
};
let leftoverVelocity = {
  x: 0,
  y: 0
}

// Play mode variables -------
let playMode = true;
const pauseInPlay = false;

// Edge mode variables -------
const edgeModes = {
  PRESERVE_UNDERLYING_SOURCE: 'preserve_underlying_source', // "chunk mode"
  STRETCH_EDGE_PIXELS: 'stretch_edge_pixels',
  WRAP_EDGE_PIXELS: 'wrap_edge_pixels', 
}
const edgeModeVelocityOffset = {
  [edgeModes.PRESERVE_UNDERLYING_SOURCE]: 2,
  [edgeModes.STRETCH_EDGE_PIXELS]: 1,
  [edgeModes.WRAP_EDGE_PIXELS]: 1,
}
let currEdgeMode = edgeModes.PRESERVE_UNDERLYING_SOURCE;
let topBottomEdge, leftRightEdge;

// Source mode variables -------
const sourceModes = {
  VIDEO: 'video',
  FREEZE: 'freeze'
}
let currSourceMode = sourceModes.VIDEO;

// -------

function setup() {
  pixelDensity(1);
  frameRate(30);
  
  c = createCanvas(vWidth, vHeight);
  vid = createVideo(vidTitle);
  /*
    by default video shows up in separate dom
    element. hide it and draw it to the canvas instead
  */
  vid.hide();
  recordAndAppend();
}

function drawVideoFrame() {
  image(vid, 0, 0, vWidth, vHeight);
}

let wrap = {
  x: 0,
  y: 0,
}

function getMainChunk(pixelOffset) {
    return get(
      0,
      0,
      vWidth - pixelOffset.x,
      vHeight - pixelOffset.y
    );
}

function drawMainChunk(main, pixelOffset) {
    copy(main,
      0, 0, vWidth - pixelOffset.x, vHeight - pixelOffset.y, 
      0 + pixelOffset.x, 0 + pixelOffset.y, vWidth - pixelOffset.x, vHeight - pixelOffset.y
    )
}

function drawTBChunk(chunk, pixelOffset) {
    // redraw tb edge 0, 0 full width, po.y
  if (pixelOffset.y === 0) {
    return;
  }
  
  if (pixelOffset.y > 0) {
      copy(chunk,
        0, 0, vWidth, Math.abs(pixelOffset.y),
        0, vHeight - pixelOffset.y, vWidth, Math.abs(pixelOffset.y),
      )
      
      return;
  }
  
  copy(chunk,
    0, 0, vWidth, Math.abs(pixelOffset.y),
    0, 0, vWidth, Math.abs(pixelOffset.y),
  )
}

function drawLRChunk(chunk, pixelOffset) {
    // redraw lr edge 0, 0 po.x, full height 
    
  if (pixelOffset.x === 0) {
    return;
  }
  
  if (pixelOffset.x > 0) {
    copy(chunk,
      0, 0, Math.abs(pixelOffset.x), vHeight,
      vWidth - pixelOffset.x, 0, Math.abs(pixelOffset.x), vHeight,
    )
    
    return;
  }
  

  copy(chunk,
    0, 0, Math.abs(pixelOffset.x), vHeight,
    0, 0, Math.abs(pixelOffset.x), vHeight,
  )
}

function draw() {
  if (playMode) {
    image(vid, 0, 0, vWidth, vHeight);
    return;
  }
  
  const pixelOffset = computePixelOffset();
  
  
  if (currEdgeMode === edgeModes.WRAP_EDGE_PIXELS) {
    drawVideoFrame();
    
    // find out what the last offset values were 
    // update those values to reflect the new addition 
    wrap.x = (wrap.x + pixelOffset.x) % vWidth;
    wrap.y = (wrap.y + pixelOffset.y) % vHeight;
  }
  
  // Get the part of the last frame that will be
  // preserved and shifted in this frame.
  // Note: Do this before modifying anything this
  // frame so that we have the final state from
  // last frame.
  let main = currEdgeMode === edgeModes.WRAP_EDGE_PIXELS
    ? get(
      wrap.x,
      wrap.y,
      vWidth - wrap.x,
      vHeight - wrap.y
    )
    : get(
      pixelOffset.x,
      pixelOffset.y,
      vWidth - pixelOffset.x,
      vHeight - pixelOffset.y
    );

  // If we are in stretch edge pixel mode we need to get
  // the edges of the last frame so that we can stretch
  // them out later
  if (currEdgeMode === edgeModes.STRETCH_EDGE_PIXELS) {
    getEdges(pixelOffset);
  }
  
  if (currEdgeMode === edgeModes.WRAP_EDGE_PIXELS) {
    getEdges(wrap, wrap)
  }

  // If the source mode is "video" then we want to play
  // the video under the frozen part of the screen and let
  // the new pixels from video come in.
  if (currSourceMode === sourceModes.VIDEO) {
    image(vid, 0, 0, vWidth, vHeight);
  }

  // Copy the part of the last frame that will be
  // preserved into its new shifted position.
  copy(main,
    0, 0, vWidth - pixelOffset.x, vHeight - pixelOffset.y, 
    0, 0, vWidth - pixelOffset.x, vHeight - pixelOffset.y
  )
  
  if (currEdgeMode === edgeModes.WRAP_EDGE_PIXELS) {
    drawTBChunk(topBottomEdge, wrap);
    drawLRChunk(leftRightEdge, wrap);
  }

  if (currEdgeMode === edgeModes.STRETCH_EDGE_PIXELS) {
    stretchEdges(pixelOffset);
  }
}

function mousePressed() {
  vid.loop(); // set the video to loop and start playing
}
 
function keyPressed() {
  switch (keyCode) {
    case 32: // space bar
      playMode = !playMode;
      
      if (pauseInPlay) {
        playMode ? vid.loop() : vid.pause();
      }

      break;

    case 13: // enter
      // should this also reset the fib function?
      currentVelocity.x = 0;
      currentVelocity.y = 0;
      break;
    
    case 37: // left arrow
      currentVelocity.x = computeVelocityLeftRight(-edgeModeVelocityOffset[currEdgeMode]);
      logVel(`Vel: ${currentVelocity.x}, ${currentVelocity.y}`);
      break;
    
    case 38: // up arrow
      currentVelocity.y = computeVelocityUpDown(edgeModeVelocityOffset[currEdgeMode]);
      logVel(`Vel: ${currentVelocity.x}, ${currentVelocity.y}`);
      break;

    case 39: // right arrow
      currentVelocity.x = computeVelocityLeftRight(edgeModeVelocityOffset[currEdgeMode]);
      logVel(`Vel: ${currentVelocity.x}, ${currentVelocity.y}`);
      break;

    case 40: // down arrow
      currentVelocity.y = computeVelocityUpDown(-edgeModeVelocityOffset[currEdgeMode]);
      logVel(`Vel: ${currentVelocity.x}, ${currentVelocity.y}`);
      break;

    case 86: // v
      currSourceMode = sourceModes.VIDEO;
      
      if (pauseInPlay) {
        vid.loop();
      }
      break;
      
    case 87: // w
      currEdgeMode = edgeModes.WRAP_EDGE_PIXELS;
      break;

    case 70: // f
      currSourceMode = sourceModes.FREEZE;
      
      if (pauseInPlay) {
        vid.pause();
      }
      
      break;

    case 67: // c
      currEdgeMode = edgeModes.PRESERVE_UNDERLYING_SOURCE;
      break;

    case 83: // s
      currEdgeMode = edgeModes.STRETCH_EDGE_PIXELS;
      break;
  }
}

// -------

function computeVelocityUpDown(offset) {
  return getVelocityUpDown.next(offset).value * globalVelocityScaler;
}

function computeVelocityLeftRight(offset) {
  return getVelocityLeftRight.next(offset).value * globalVelocityScaler;
}

// Computes and returns the pixel offset for this frame based
// on the globals currentVelocity and leftoverVelocity
function computePixelOffset() {
  let pixelOffset = {
    x: currentVelocity.x + leftoverVelocity.x,
    y: currentVelocity.y + leftoverVelocity.y
  }

  leftoverVelocity.x = pixelOffset.x % 1;
  leftoverVelocity.y = pixelOffset.y % 1;

  pixelOffset.x = Math.floor(pixelOffset.x);
  pixelOffset.y = Math.floor(pixelOffset.y)

  return pixelOffset;
}

// getFibonacciVelocity with an integer 'offset' which tells
// you how many values to jump forward or backwards.
// Stretch mode can request a new velocity with a low offset
// like -1 or +1, chunk mode can request a new velocity that
// jumps more in either direction.
function * getFibonacciVelocity() {
  let fibSequence = [0, 0, 0];  // current fib value is always fibSequence[2]
  let offset = 0;

  // Internal helper function to step our fibonacci sequence once. Handles 0 case
  // and moving in positive + negative directions.
  function fibStep(dir) {
    // If the product of the direction and the current fib value are positive
    // then they are alighed and we are moving forward in our current direction.
    // If they are negative it means that we are moving backwards.
    // Examples:
    //    3       *     1       =        +
    // fib value     direction     moving forward (next fib value 5)
    //
    //    -3      *     -1      =        +
    // fib value     direction     moving forward (next fib value -5)
    //
    //    -3      *     1       =        -
    // fib value     direction     moving backwards (next fib value -2)
    let movingBackwards = (fibSequence[2] * dir) < 0;

    // Handle 0 special cases. A fibSequence of [0, 0, 0] means velocity 0,
    // otherwise we start moving up [0, 1, 1] or down [0, -1, -1]
    if (fibSequence[0] == 0) {
      // If we are at special case 0 then we need to start moving in a direction.
      if (fibSequence[2] == 0) {
        return fibSequence = dir < 0 ? [0, -1, -1] : [0, 1, 1];

      } else if (movingBackwards) {
        // We reached either [0, 1, 1] or [0, -1, -1] and are moving backwards.
        // Set fibSequence to 0 velocity, ie [0, 0, 0]
        return fibSequence = [0, 0, 0];
      }
    }

    // Regular fibonacci logic, move backwards or forwards depending dir and
    // whether we are positive or negative
    if (movingBackwards) {
      fibSequence.unshift(fibSequence[1] - fibSequence[0]);
      fibSequence.pop();
    } else {
      fibSequence.push(fibSequence[1] + fibSequence[2]);
      fibSequence.shift();
    }
  }

  while (true) {
    // Take magnitude(offset) steps in the 'direction' (positive
    // or negative) of offset
    for (let i = 0; i < Math.abs(offset); ++i) {
      fibStep(offset);
    }

    // Yield the current fib value and get the next offset
    offset = yield fibSequence[2];
  }
}

function getEdges(positiveNegative, segmentSize = { x: 1, y: 1 }) {
  if (positiveNegative.y <= 0) {
    // Top
    topBottomEdge = get(0, vHeight - Math.abs(segmentSize.y), vWidth, Math.abs(segmentSize.y));
  } else {
    // Bottom
    topBottomEdge = get(0, 0, vWidth, Math.abs(segmentSize.y));
  }

  if (positiveNegative.x <= 0) {
    // Left
    leftRightEdge = get(vWidth - Math.abs(segmentSize.x), 0, Math.abs(segmentSize.x), vHeight);
  } else {
    // Right
    leftRightEdge = get(0, 0, Math.abs(segmentSize.x), vHeight);
  }
}


function stretchEdges(pixelOffset) {
  if (Math.abs(pixelOffset.x) < Math.abs(pixelOffset.y)) {
    // Moving faster vertically, do that stretch last so that
    // you see more of it at the edges
    stretchLeftRight(pixelOffset);
    stretchTopBottom(pixelOffset);
  } else {
    // Moving faster horizontally, do that stretch last so that
    // you see more of it at the edges
    stretchTopBottom(pixelOffset);
    stretchLeftRight(pixelOffset);
  }
}

function stretchTopBottom(pixelOffset) {
  if (-1 < pixelOffset.y && pixelOffset.y < 1) {
    // Less than 1 pixel offset, don't bother stretching
    return;
  }
  if (pixelOffset.y < 0) {
    // Top
    copy(topBottomEdge,
      0, 0, vWidth, 1,
      0, 0, vWidth,  Math.abs(pixelOffset.y)
    )
  } else {
    // Bottom
    copy(topBottomEdge,
      0, 0, vWidth, 1,
      0, vHeight - pixelOffset.y, vWidth,  pixelOffset.y
    )
  }
}

function stretchLeftRight(pixelOffset) {
  if (-1 < pixelOffset.x && pixelOffset.x < 1) {
    // Less than 1 pixel offset, don't bother stretching
    return;
  }

  if (pixelOffset.x < 0) {
    // Left
    copy(leftRightEdge,
      0, 0, 1, vHeight,
      0, 0, Math.abs(pixelOffset.x), vHeight
    )
  } else {
    // Right
    copy(leftRightEdge,
      0, 0, 1, vHeight,
      vWidth - pixelOffset.x, 0, pixelOffset.x, vHeight
    )
  }
}

// This is necessary to make p5 work with browser modules
// see, for instance: https://forum.processing.org/two/discussion/24662/script-type-module-draw-and-setup-not-working
window.setup = setup;
window.draw = draw;
window.mousePressed = mousePressed;
window.keyPressed = keyPressed;