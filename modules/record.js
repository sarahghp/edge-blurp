let recorder;
let chunks;

const toggleState = (stream, button) => {
  /* if recording, stop; otherwise, record */
  if (recorder?.state === 'recording') {
    recorder.stop();
    button.textContent= '⭕️';
    return;
  }
  
  initRecord(stream);
  recorder.start();
  button.textContent = '🔴';
}

const createDownloadLink = (url) => {
  const linkTemplate = `
    <a href="${url}" download>download the file</a>
  `
  const link = document.createElement('div');
  link.innerHTML = linkTemplate;
  document.body.appendChild(link);
}

const addVideoViewer = (url) => {
  const vid = document.createElement('video');
  vid.controls = true;
  vid.src = url;
  document.body.appendChild(vid);
}

const exportVideo = () => {
  const blob = new Blob(chunks);
  const vidUrl = URL.createObjectURL(blob);
  createDownloadLink(vidUrl);
  // addVideoViewer(vidUrl);
}

const initRecord = (stream) => {
  
  recorder = new MediaRecorder(stream, { mimeType: 'video/webm\;codecs=h264' });
  chunks = [];

  recorder.ondataavailable = (evt) => {
    if (evt.data.size) {
      chunks.push(evt.data);
    }
  };
  
  recorder.onstop = exportVideo;  
}

const recordAndAppend = () => {
    
  const recordButton = document.querySelector('#record-me');
  const stream = document.querySelector('canvas').captureStream(30);

  recordButton.addEventListener('click', toggleState.bind(null, stream, recordButton));

}

export { recordAndAppend }
